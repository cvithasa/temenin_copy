from django.test import TestCase
# from django.test import Client
# from .models import Status
# from .forms import Status_Form
# from django.http import HttpRequest
# from django.urls import resolve
# from .views import index
# import unittest

# # Create your tests here.

# class UpdateStatusUnitTest(TestCase):

#     def test_update_status_url_is_exist(self):
#         response = Client().get('/status/')
#         self.assertEqual(response.status_code, 200)

#     def test_update_status_using_index_func(self):
#         found = resolve('/status/')
#         self.assertEqual(found.func, index)

#     def test_model_can_create_new_status(self):
#         new_status = Status.objects.create(activity='im dancing')

#         counting_all_available_status = Status.objects.all().count()
#         self.assertEqual(counting_all_available_status,1)

#     def test_status_success_and_render_the_result(self):
#         test = 'Anonymous'
#         response_status = Client().post('/status/add_status',{'activity':test})
#         self.assertEqual(response_status.status_code, 302)

#         response = Client().get('/status/')
#         html_response = response.content.decode('utf8')
#         self.assertIn(test, html_response)

#     def test_status_error_and_render_the_result(self):
#         test = ''
#         response_status = Client().post('/status/add_status',{'activity':test})
#         self.assertEqual(response_status.status_code, 302)

#         response = Client().get('/status/')
#         html_response = response.content.decode('utf8')
#         self.assertIn(test, html_response)