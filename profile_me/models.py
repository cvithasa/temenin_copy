from django.db import models

# Create your models here.

class User(models.Model):
    nama = models.CharField(max_length = 100)
    npm = models.CharField(max_length = 100)
    keahlian = models.TextField()
    tingkat_keahlian = models.TextField()
    emailku = models.EmailField()
    tampilkan_nilai = models.BooleanField()
    profile_linkedin = models.URLField()

    def to_dict(self):
        result = {}
        result['nama'] = self.nama
        result['npm'] = self.npm
        result['email'] = self.emailku
        result['tampilkan_nilai'] = self.tampilkan_nilai
        result['profile_link'] = self.profile_linkedin

        # pisahkan semua keahlian menjadi list
        keahlianku = []
        hasil = ''
        for i in self.keahlian:
            if i == '+':
                keahlianku.append(hasil)
                hasil = ''
            else :
                hasil+=i
        keahlianku.append(hasil)

        # pisahkan semua tingkatkeahlian menjadi list juga
        tingkat = []
        hasil = ''
        for i in self.tingkat_keahlian:
            if i == "+":
                tingkat.append(hasil)
                hasil = ''
            else :
                hasil += i
        tingkat.append(hasil)
        ahlitingkat = []
        counter = 0
        for i in range(len(keahlianku)):
            ahlitingkat.append([])
            ahlitingkat[counter].append(keahlianku[i])
            ahlitingkat[counter].append(tingkat[i])
            counter+=1
        result['keahlian'] = ahlitingkat
        return result