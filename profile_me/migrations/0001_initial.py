# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100)),
                ('npm', models.CharField(max_length=100)),
                ('keahlian', models.TextField()),
                ('tingkat_keahlian', models.TextField()),
                ('emailku', models.EmailField(max_length=254)),
                ('tampilkan_nilai', models.BooleanField()),
                ('profile_linkedin', models.URLField()),
            ],
        ),
    ]
