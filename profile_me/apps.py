from django.apps import AppConfig


class ProfileMeConfig(AppConfig):
    name = 'profile_me'
