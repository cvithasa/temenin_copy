from django.conf.urls import url, include
from .views import index, update, add_status, delete_status
 
urlpatterns = [

    url(r'^$', index, name = "index"),
    url(r'^update/', update, name = "update"),
	url(r'^add_status', add_status, name='add_status'),
	url(r'^delete_status/(?P<id_status>\d+)/$', delete_status, name = 'delete_status'),
]
