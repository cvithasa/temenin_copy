from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from .models import User
import requests
from django.shortcuts import render
from update_status.models import StatusKu
from update_status.forms import Form_status
from django.http import HttpResponseRedirect

# Create your views here.

RIWAYAT_API = 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'
response = {}

def index(request):
    if request.session['user_login'] is None:
        response['login'] = False
        response['logintrue'] = False
        return HttpResponseRedirect(reverse('login:auth_login'))
    else : 
        html = 'profile.html'
        boo = request.session['kode_identitas']
        user = User.objects.filter(npm = boo)
        response = {}
        if len(user) > 1:
            response = user[0].to_dict()
        else:
            usernew = create_user(request)
            response = usernew.to_dict()
        response['riwayats'] = get_riwayat()
        # TODO ngambil rahasia dari objek mahasiswa
        set_data_for_session(request)
        kode_identitas = get_data_user(request, 'kode_identitas')
        response['test'] = 'hai halo'
        response['login'] = True
        statusnya = StatusKu.objects.order_by('-id')
        response['status_form'] = Form_status
        response['status'] = statusnya
        response['logintrue'] = True
        return render(request, html, response)

# -------------- creating user if not exist in database ----------------------- #
def create_user(requests) :
    print('->create_user')
    creature = User(nama=requests.session['user_login'], npm=requests.session['kode_identitas'], keahlian = "", tingkat_keahlian="", emailku="", tampilkan_nilai = True, profile_linkedin = "")
    creature.save()
    return creature
# -------------- creating user if not exist in database ----------------------- #

def add_status(request):
    form = Form_status(request.POST or None)
    set_data_for_session(request)
    kode_identitas = get_data_user(request, 'kode_identitas')
    pengguna = User.objects.get(npm = kode_identitas)
    if(request.method == 'POST' and form.is_valid()):
        print('dia adalah=>',pengguna)
        response['description'] = request.POST['description']
        status = StatusKu(pengguna = pengguna, description=response['description'])
        #status.pengguna = pengguna
        status.save()
        return HttpResponseRedirect('/status/')
    else:
        return HttpResponseRedirect('/status/')

def delete_status(request, id_status):
    status = StatusKu.objects.get(pk = id_status)
    status.delete()
    return HttpResponseRedirect('/status/')

def get_data_user(request, tipe):
    data = None
    if tipe == "user_login" and 'user_login' in request.session:
        data = request.session['user_login']
    elif tipe == "kode_identitas" and 'kode_identitas' in request.session:
        data = request.session['kode_identitas']
    elif tipe == "role" and 'role' in request.session:
        data = request.session['role']
    return data

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def get_riwayat():
	riwayat = requests.get(RIWAYAT_API)
	return riwayat.json()

# ------------------ Update profile ------------------------ #
def update(request):
    html = "update.html"
    return render(request, html, response)

def updatedata(request):
    if request.method == 'POST':
        hehe =1

    return HttpResponseRedirect(reverse('profile_me:index'))
