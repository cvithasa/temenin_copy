# from django.http import HttpRequest
# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import index, response as response_index
# from .csui_helper import get_access_token, get_data_user, get_client_id, verify_user

# import environ
# import requests

# root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
# env = environ.Env(DEBUG=(bool, False), )
# environ.Env.read_env('.env')


# # Create your tests here.
# class Lab9UnitTest(TestCase):
#     def setUp(self):
#         self.username = env("SSO_USERNAME")
#         self.password = env("SSO_PASSWORD")

#     # views test
#     def test_lab_9_url_is_exist(self):
#         response = Client().get('/lab-9/')
#         self.assertEqual(response.status_code, 200)

#     def test_lab_9_using_index_func(self):
#         found = resolve('/login/')
#         self.assertEqual(found.func, index)

#     def test_lab_9_index_condition(self):
#         response = self.client.get('/login/')
#         self.assertEqual(response.status_code, 200)

#         # logged in, redirect to profile page
#         self.assertNotIn("user_login", self.client.session)
#         html_content = response.content.decode('utf-8')
#         self.assertIn("Halaman Login", html_content)

#         response = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
#         self.assertEqual(response.status_code, 302)

#         login = self.client.get('/login/')
#         self.assertEqual(login.status_code, 302)

#     def test_profile(self):
#         # Not logged in
#         response = self.client.get('/login/profile/')
#         self.assertEqual(response.status_code, 302)

#         # Logged in
#         self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
#         response = self.client.get('/login/profile/')
#         self.assertEqual(response.status_code, 200))

#     # custom_auth test
#     def test_auth_login(self):
#         login_1 = self.client.post('/login/custom_auth/login')
#         self.assertEqual(login_1.status_code, 301)
#         self.assertRaisesMessage(login_1, "Username atau password salah")
#         login_1 = self.client.get('/login/custom_auth/login')
#         self.assertRaisesMessage(login_1, "Username atau password salah")

#         login_2 = self.client.post('/login/custom_auth/login/', {'username': self.username, 'password': self.password})
#         self.assertRaisesMessage("Anda berhasil login", login_2)
#         self.assertEqual(login_2.status_code, 302)

#     def test_auth_logout(self):
#         login = self.client.post('/login/custom_auth/login/', {"username": self.username, "password": self.password})
#         logout = self.client.get('/login/custom_auth/logout/')
#         self.assertEqual(logout.status_code, 302)
#         self.assertRaisesMessage("Anda berhasil logout. Semua session Anda sudah dihapus", logout)

#     # csui_helper test
#     def test_get_access_token(self):
#         username = "aku cantik"
#         password = "oke"

#         test_1 = get_access_token(username, password)
#         self.assertTrue(type(test_1), str)
#         self.assertIsNone(test_1)
#         self.assertRaises(Exception, get_access_token(username, password))

#         test_2 = get_access_token(self.username, self.password)
#         self.assertIsNotNone(test_2)

#     def test_get_client_id(self):
#         client_id = get_client_id()
#         self.assertTrue(type(client_id), str)
#         self.assertEqual(client_id, "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

#     def test_verify_id(self):
#         verified = verify_user("1234567890")
#         self.assertIn("error_description", verified)

#     def test_get_data_user(self):
#         access_token = '123456789'
#         id = 'test'
#         data_user = get_data_user(access_token, id)
#         self.assertIn("detail", data_user)
#         self.assertEqual(data_user["detail"], "Authentication credentials were not provided.")
